package ee.sarv;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args)  throws IOException {
	// write your code here

        File f1 = new File("esimene.txt");
        File f2 = new File( "teine.txt");

        // faili salvestamine 1. variant
        FileOutputStream fos = new FileOutputStream(f1);
        OutputStreamWriter osw = new OutputStreamWriter(fos);
        BufferedWriter bw = new BufferedWriter(osw);
        for (int i = 0; i < 10; i++) {
            bw.write(String.format("Rida %s on see\n", i));
        }
        bw.close();

        System.out.printf("\ntekitati fail %s\n", f1.getAbsolutePath());

        // faili salvestamine 2. variant
        FileWriter fw = new FileWriter("teine.txt");
        for (int i = 0; i < 10; i++) {
            fw.write(String.format("Rida nr %s;\n", i));
        }
        fw.close();

        System.out.printf("\ntekitati fail %s\n", f2.getAbsolutePath());


        System.out.printf("\nvariant 1 loeme failist %s:\n", f1.getName());
        // failist lugemise 1. variant
        FileInputStream fis = new FileInputStream(f1);
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);
        for (String line = null; (line = br.readLine()) != null;) {
            System.out.println(line);
        }

        System.out.printf("\nvariant 2 loeme failist %s:\n", f1.getName());
        // failist lugemise 2. variant

        FileReader fr = new FileReader(f1);
        BufferedReader br2 = new BufferedReader(fr);
        for (String line = null; (line = br2.readLine()) != null; ) {
            System.out.println(line);
        }

        System.out.printf("\nvariant 3 loeme failist %s:\n", f2.getName());

        // failist lugemise üks uuem variant
        String[] vastuseRead = Files.lines(f2.toPath()).toArray(x -> new String[x]);
        for (var rida:vastuseRead ) {

            System.out.println(rida);
        }


    }
}
